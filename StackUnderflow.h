#pragma once

#include <exception>

class StackUnderflow : public std::exception
{
public:
	StackUnderflow() : reason_("Stack Underflow!") {}
	const char* what() const noexcept override { return reason_; }
private:
	const char* reason_;
};