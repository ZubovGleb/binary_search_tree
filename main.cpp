#include "BinarySearchTree.h"
#include <iostream>


int main()
{
    BinarySearchTree<int> tree1;
    std::cout << "Creating the first tree...\n" << "Number of elements in the first tree: " << tree1.getCount() << '\n';

    std::cout << "Filling the tree with 7 9 12 5 6 8 13" << '\n';
    tree1.insert(7);
    tree1.insert(9);
    tree1.insert(12);
    tree1.insert(5);
    tree1.insert(6);
    tree1.insert(8);
    tree1.insert(13);
    tree1.print(std::cout);
    std::cout << '\n';
    std::cout << '\n' << "Is 20 in the tree?\n" << tree1.iterativeSearch(20) << '\n';
    std::cout << "Is 12 in the tree?\n" << tree1.iterativeSearch(12) << '\n' << '\n';
    std::cout << "Delete 7, 9, 13\n";
    tree1.deleteKey(7);
    tree1.deleteKey(9);
    tree1.deleteKey(13);
    tree1.print(std::cout);
    std::cout << '\n';
    std::cout << "Number of elements in the first tree: " << tree1.getCount() << '\n';
    std::cout << "Tree height: " << tree1.getHeight() << '\n';

    std::cout << "Inorder walk (recursive): ";
    tree1.inorderWalk();
    std::cout << '\n';
    std::cout << "Iterative inorder walk: ";
    tree1.iterativeInorderWalk();
    std::cout << '\n';
    std::cout << "Walk by levels: ";
    tree1.walkByLevels();
    std::cout << '\n';

    std::cout << "Creating a second tree similar to the first one" << '\n';
    BinarySearchTree<int> tree2;
    tree2.insert(8);
    tree2.insert(5);
    tree2.insert(12);
    tree2.insert(6);

    std::cout << "Tree1 and tree2 are similar? " << tree1.isSimilar(tree2) << '\n';
    std::cout << "Tree1 and tree2 have identical key/keys? " << tree1.isIdenticalKey(tree2) << '\n';

    std::cout << "Adding 91 to the second tree..." << '\n';
    tree2.insert(91);

    std::cout << "Tree1 and tree2 are similar? " << tree1.isSimilar(tree2) << '\n';
    std::cout << "Tree1 and tree2 have identical key/keys? " << tree1.isIdenticalKey(tree2) << '\n';

    return 0;
}
