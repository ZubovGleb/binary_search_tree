#pragma once

#include <exception>

class QueueUnderflow : public std::exception
{
public:
	QueueUnderflow() : reason_("Queue Underflow!") {}
	const char* what() const noexcept override { return reason_; }
private:
	const char* reason_;
};