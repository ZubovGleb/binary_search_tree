#pragma once

#include <ostream>
#include "LimitedSizeQueue.h"
#include "LimitedSizeStack.h"

template<class T>
class BinarySearchTree {
public:
    BinarySearchTree();
    BinarySearchTree(const BinarySearchTree<T>& scr) = delete;
    BinarySearchTree(BinarySearchTree<T>&& scr) noexcept;
    BinarySearchTree<T>& operator=(const BinarySearchTree<T>& src) = delete;
    BinarySearchTree<T>& operator=(BinarySearchTree<T>&& src) noexcept;
    virtual ~BinarySearchTree();

    bool iterativeSearch(const T& key) const;
    bool insert(const T& key);
    bool deleteKey(const T& key);
    void print(std::ostream& out) const;
    int getCount() const;
    int getHeight() const;
    void iterativeInorderWalk() const;
    void inorderWalk() const;
    void walkByLevels() const;
    bool isSimilar(const BinarySearchTree<T>& other) const;
    bool isIdenticalKey(const BinarySearchTree<T>& other) const;

private:
    template<class T>
    struct Node {
        T key_;
        Node<T>* left_;
        Node<T>* right_;
        Node<T>* p_;

        Node(T key, Node<T>* left = nullptr, Node<T>* right = nullptr, Node<T>* p = nullptr) :
            key_(key), left_(left), right_(right), p_(p) {}
    };

    Node<T>* root_;

    void deleteSubtree(Node<T>* node);
    Node<T>* iterativeSearchNode(const T& key) const; 

    void printNode(std::ostream& out, Node<T>* root) const;

    int getCount(const Node<T>* node) const;
    int getHeight(Node<T>* node) const; 
    void inorderWalk(Node<T>* node) const;
    bool isSimilar(Node<T>* root, const BinarySearchTree<T>& other) const;
    bool isIdenticalKey(Node<T>* root, const BinarySearchTree<T>& other) const;
};

template<class T>
BinarySearchTree<T>::BinarySearchTree() :
    root_(nullptr) {}

template<class T>
BinarySearchTree<T>::BinarySearchTree(BinarySearchTree<T>&& scr) noexcept :
    root_(scr.root_) {
    scr.root_ = nullptr;
}

template<class T>
BinarySearchTree<T>& BinarySearchTree<T>::operator=(BinarySearchTree<T>&& src) noexcept {
    if (this != &src) {
        swap(root_,src->root_);
    }
    return *this;
}

template<class T>
BinarySearchTree<T>::~BinarySearchTree() {
    deleteSubtree(root_);
}

template<class T>
void BinarySearchTree<T>::deleteSubtree(Node<T>* node) {
    if (node == nullptr) {
        return;
    }
    deleteSubtree(node->left_);
    deleteSubtree(node->right_);
    delete node;
}

template<class T>
bool BinarySearchTree<T>::iterativeSearch(const T& key) const {
    return (iterativeSearchNode(key) != nullptr);
}

template<class T>
class BinarySearchTree<T>::Node<T>* BinarySearchTree<T>::iterativeSearchNode(const T& key) const {
    Node<T>* temp = root_;
    while (temp != nullptr && temp->key_ != key) {
        if (temp->key_ > key) 
        {
            temp = temp->left_;
        }
        else 
        {
            temp = temp->right_;
        }
    }
    return temp;
}

template<class T>
bool BinarySearchTree<T>::insert(const T& key) {
    if (root_ == nullptr)
    {
        root_ = new Node<T>(key);
        return true;
    }
    Node<T>* temp = root_;
    while (key != temp->key_) 
    {
        if (key < temp->key_)
        {
            if (temp->left_ == nullptr)
            {
                temp->left_ = new Node<T>(key, nullptr, nullptr, temp);
                return true;
            }
            temp = temp->left_;
        }
        else
        {
            if (temp->right_ == nullptr)
            {
                temp->right_ = new Node<T>(key, nullptr, nullptr, temp);
                return true;
            }
            temp = temp->right_;
        }
    }
    return false;
}

template<class T>
bool BinarySearchTree<T>::deleteKey(const T& key) {
    Node<T>* temp = iterativeSearchNode(key);
    if (temp == nullptr)
    {
        return false;
    }
    Node<T>* tempParent = temp->p_;
    if (temp->left_ == nullptr && temp->right_ == nullptr) {
        if (tempParent == nullptr)
        {
            root_ = nullptr;
            delete temp;
            return true;
        }
        if (tempParent->key_ > temp->key_)
        {
            tempParent->left_ = nullptr;
        }
        else
        {
            tempParent->right_ = nullptr;
        }
        delete temp;
        return true;
    }
    
    if (temp->left_ != nullptr && temp->right_ != nullptr) {
        Node<T>* min = temp->right_;
        while (min->left_ != nullptr) {
            min = min->left_;
        }
        temp->key_ = min->key_;
        if (min->p_->key_ == temp->key_) 
        {
            temp->right_ = nullptr;
        }
        else
        {
            min->p_->left_ = nullptr;
        }
        delete min;
        return true;
    }
    
    if (tempParent == nullptr)
    {
        if (temp->left_ == nullptr) {
            root_ = temp->right_;
        }
        else {
            root_ = temp->left_;
        }
        delete temp;
        root_->p_ = nullptr;
        return true;
    }
    if (temp->left_ != nullptr) {
        temp = temp->left_;
    }
    else {
        temp = temp->right_;
    }
    delete temp->p_;
    temp->p_ = tempParent;
    if (tempParent->key_ > temp->key_) {
        tempParent->left_ = temp;
    }
    else {
        tempParent->right_ = temp;
    }
    return true;
}

template<class T>
void BinarySearchTree<T>::printNode(std::ostream& out, Node<T>* root) const {
    if (root != nullptr) {
	    out << root->key_;
		if (root->left_ || root->right_) {
			out << '(';
			if (root->left_) { 
				printNode(out, root->left_); 
			}
			out << ')' << '(';
			if (root->right_) { 
				printNode(out, root->right_); 
			}
			out << ')';
		}
	}
}

template <class T>
void BinarySearchTree<T>::print(std::ostream& out) const { 
    printNode(out, root_); 
}

template<class T>
int BinarySearchTree<T>::getCount() const {
    return getCount(this->root_);
}

template<class T>
int BinarySearchTree<T>::getCount(const Node<T>* node) const {
    if (node == nullptr) {
        return 0;
    }
    return (1 + getCount(node->left_) + getCount(node->right_));
}

template<class T>
int BinarySearchTree<T>::getHeight() const {
    if (root_ == nullptr) {
        return 0;
    }
    return getHeight(root_);
}

template<class T>
int BinarySearchTree<T>::getHeight(Node<T>* node) const {
    if (node->left_ == nullptr && node->right_ == nullptr)
    {
        return 1;
    }
    if (node->left_ != nullptr && node->right_ != nullptr)
    {
        return std::max(getHeight(node->left_) + 1, getHeight(node->right_) + 1);
    }
    if (node->left_ != nullptr)
    {
        return getHeight(node->left_) + 1;
    }
    if (node->right_ != nullptr)
    {
        return getHeight(node->right_) + 1;
    }
}

template <class T>
void BinarySearchTree<T>::inorderWalk(Node<T>* node) const {
    if (node != nullptr) {
        this->inorderWalk(node->left_);
        std::cout << node->key_ << ' ';
        this->inorderWalk(node->right_);
    }
}

template <class T>
void BinarySearchTree<T>::inorderWalk() const {
    this->inorderWalk(root_);
}

template <class T>
void BinarySearchTree<T>::iterativeInorderWalk() const {
    StackArray<Node<T>*> stack(this->getHeight());
    Node<T>* currentNode = root_;
    while (!stack.isEmpty() || currentNode != nullptr) {
        if (currentNode != nullptr)
        {
            stack.push(currentNode);
            currentNode = currentNode->left_;
        }
        else
        {
            currentNode = stack.pop();
            std::cout << currentNode->key_ << ' ';
            currentNode = currentNode->right_;
        }
    }
}

template <class T>
void BinarySearchTree<T>::walkByLevels() const {
    if (root_ == nullptr)
    { 
        return; 
    }
    Node<T>* currentNode = nullptr;
    QueueArray<Node<T>*> queue(getCount());
    queue.enQueue(root_);
    while (!queue.isEmpty()) {
        currentNode = queue.deQueue();
        std::cout << currentNode->key_ << ' ';

        if (currentNode->left_ != nullptr)
        { 
            queue.enQueue(currentNode->left_);
        }
        if (currentNode->right_ != nullptr)
        { 
            queue.enQueue(currentNode->right_);
        }
    }
}

template <class T>
bool BinarySearchTree<T>::isSimilar(const BinarySearchTree<T>& other) const {
    if (root_ == nullptr) 
    {
        if (other.root_ != nullptr)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    if (other.root_ == nullptr || getCount() != other.getCount())
        return false;

    return isSimilar(root_, other);
}

template<class T>
bool BinarySearchTree<T>::isSimilar(Node<T>* root, const BinarySearchTree<T>& other) const {
    if (root != nullptr) {
        if (isSimilar(root->left_, other)) {
            if (other.iterativeSearch(root->key_)) {
                if (isSimilar(root->right_, other)) {
                    return true;
                }
            }
        }
        return false;
    }
    return true;
}

template <class T>
bool BinarySearchTree<T>::isIdenticalKey(const BinarySearchTree<T>& other) const {
    if (root_ == nullptr || other.root_ == nullptr)
    { 
        return false; 
    }
    return this->isIdenticalKey(root_, other);
}

template <class T>
bool BinarySearchTree<T>::isIdenticalKey(Node<T>* root, const BinarySearchTree<T>& other) const {
    if (root != nullptr) {
        if (isIdenticalKey(root->left_, other)) {
            if (other.iterativeSearch(root->key_)) {
                return true;
            }
            if (isIdenticalKey(root->right_, other)) {
                if (other.iterativeSearch(root->key_)) {
                    return true;
                }
            }
        }
        return false;
    }
    return true;
}





