#pragma once

#include <iostream>
#include "Stack.h"
#include "WrongStackSize.h"
#include "StackOverflow.h"
#include "StackUnderflow.h"

template<class T>
class StackArray : public Stack<T>
{
public:
    StackArray(size_t size = 100);
    StackArray(const StackArray<T>& src) = delete;
    StackArray(StackArray<T>&& src) noexcept;
    StackArray<T>& operator=(const StackArray<T>& src) = delete;
    StackArray<T>& operator=(StackArray<T>&& src) noexcept;
    virtual ~StackArray();
    void push(const T& e);
    const T& pop();
    bool isEmpty();
private:
    T* array_;
    size_t top_{};
    size_t size_{};
    void swap(StackArray<T>& src);
};

template<class T>
StackArray<T>::StackArray(size_t size) :
    size_(size),
    top_(0)
{
    try
    {
        array_ = new T[size + 1];
    }
    catch (...)
    {
        throw WrongStackSize();
    }
}

template<class T>
StackArray<T>::StackArray(StackArray<T>&& src) noexcept
{
    swap(src);
}

template<class T>
StackArray<T>& StackArray<T>::operator=(StackArray<T>&& src) noexcept
{
    if (this == &src) {
        return *this;
    }
    swap(src);
    return *this;
}

template<class T>
void StackArray<T>::swap(StackArray<T>& src)
{
    std::swap(array_, src.array_);
    std::swap(top_, src.top_);
    std::swap(size_, src.size_);
}

template<class T>
void StackArray<T>::push(const T& e)
{
    if (top_ == size_) {
        throw StackOverflow();
    }
    array_[++top_] = e;
}


template<class T>
const T& StackArray<T>::pop()
{
    if (isEmpty()) {
        throw StackUnderflow();
    }
    return array_[top_--];
}

template<class T>
bool StackArray<T>::isEmpty()
{
    return (top_ == 0);
}

template<class T>
StackArray<T>::~StackArray()
{
    delete[] array_;
}
