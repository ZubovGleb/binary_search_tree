#pragma once

#include "Queue.h"
#include "QueueOverflow.h"
#include "QueueUnderflow.h"
#include "WrongQueueSize.h"
#include <iostream>

template<class T>
class QueueArray : public Queue<T> {
public:
    QueueArray(size_t size);
    QueueArray(const QueueArray<T>& src) = delete;
    QueueArray(QueueArray<T>&& src) noexcept;
    QueueArray<T>& operator=(QueueArray<T>& src) = delete;
    QueueArray<T>& operator=(QueueArray<T>&& src) noexcept;
    virtual ~QueueArray();

    template<class O>
    friend std::ostream& operator<<(std::ostream& out, const QueueArray<O>& queue);
    void enQueue(const T& e);
    T deQueue();
    bool isEmpty();

private:
    T* array_;
    size_t head_;
    size_t tail_;
    size_t size_;
    size_t count_;

    void swap(QueueArray<T>& right);
};

template<class T>
QueueArray<T>::QueueArray(size_t size) :
    size_(size), head_(0), tail_(0), count_(0) {
    try {
        array_ = new T[size + 1];
    }
    catch (...) {
        throw WrongQueueSize();
    }
}

template<class T>
void QueueArray<T>::swap(QueueArray<T>& right) {
    std::swap(array_, right.array_);
    std::swap(head_, right.head_);
    std::swap(tail_, right.tail_);
    std::swap(size_, right.size_);
    std::swap(count_, right.count_);
}

template<class T>
QueueArray<T>::QueueArray(QueueArray<T>&& src) noexcept {
    swap(src);
}

template<class T>
QueueArray<T>& QueueArray<T>::operator=(QueueArray<T>&& src) noexcept {
    if (this == &src) {
        return *this;
    }
    swap(src);
    return *this;
}

template<class T>
QueueArray<T>::~QueueArray() = default;

template<class T>
void QueueArray<T>::enQueue(const T& e) {
    if (count_ == size_) {
        throw QueueOverflow();
    }
    array_[(++tail_) % (size_ + 1) + (tail_ / size_)] = e;
    count_++;
}

template<class T>
T QueueArray<T>::deQueue() {
    if (isEmpty()) {
        throw QueueUnderflow();
    }
    count_--;
    return array_[(++head_) % (size_ + 1) + (head_ / size_)];
}

template<class T>
std::ostream& operator<<(std::ostream& out, const QueueArray<T>& queue) {
    if (queue.count_ != 0) {
        for (size_t i = queue.head_; i <= queue.tail_; i++) {
            out << queue.array_[i] << " ";
        }
    }
    else
        out << "";
    return out;
}

template<class T>
bool QueueArray<T>::isEmpty()
{
    return count_ == 0;
}
