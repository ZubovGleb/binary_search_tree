#pragma once

#include <exception>

class StackOverflow : public std::exception
{
public:
	StackOverflow() : reason_("Stack Overflow!") {}
	const char* what() const noexcept override { return reason_; }
private:
	const char* reason_;
};